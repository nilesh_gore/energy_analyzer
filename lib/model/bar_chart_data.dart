class BarChartData {
  final String month;
  final int usage;

  BarChartData(
    this.month,
    this.usage,
  );
}
