class PieChartData {
  final String site;
  final int usage;

  PieChartData(
    this.site,
    this.usage,
  );
}
