import 'package:energy_analyzer/screens/barchart/bar_chart_page_1.dart';
import 'package:energy_analyzer/screens/spark_line_chart/spark_line_chart_page_1.dart';
import 'package:energy_analyzer/screens/stackedbarchart/stacked_bar_chart_page_1.dart';
import 'package:energy_analyzer/screens/top_page.dart';
import 'package:flutter/material.dart';

double? width;
double? height;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          title: Text(
            "Energy Analyzer",
          ),
          elevation: 0,
          backgroundColor: Colors.green,
          foregroundColor: Colors.green,
          shadowColor: Colors.green,
          actions: [IconButton(onPressed: () {}, icon: Icon(Icons.settings))],
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              TopPage(),
              // PieChartPageOne(),
              // HorizontalBarChartPageOne(),
              StackedBarChartPageOne(),
              BarChartPageOne(),
              SparkLineChartPageOne(),
              // HeatMapPageOne(),
              // CalenderChartPageOne(),
            ],
          ),
        ));
  }
}
