import 'package:energy_analyzer/model/bar_chart_data.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

double? width;
double? height;

class BarChartPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
      height: 450, //400
      child: Card(
          child: Column(
        children: [
          Flexible(
            child: ListTile(
              title: Text("Predicted Usage"),
              subtitle: Text("30% More Then Predicted"),
            ),
            flex: 2,
          ),
          Flexible(
            child: ListTile(
              title: Text("Till Now 5400 Unites | Predicted 2300 Unites"),
            ),
            flex: 1,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: BarChart(),
            ),
            flex: 5,
          ),
        ],
      )),
    );
  }
}

class BarChart extends StatelessWidget {
  final bool animate = true;

  @override
  Widget build(BuildContext context) {
    return charts.BarChart(
      createChartData(),
      animate: animate,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
      domainAxis: charts.OrdinalAxisSpec(
        renderSpec: charts.SmallTickRendererSpec(labelRotation: 60),
      ),
    );
  }

  List<charts.Series<BarChartData, String>> createChartData() {
    return [
      charts.Series<BarChartData, String>(
          id: 'Month Use',
          domainFn: (BarChartData data, _) => data.month,
          measureFn: (BarChartData data, _) => data.usage,
          data: getMonthUsage(),
          labelAccessorFn: (BarChartData data, _) => "",
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault),
    ];
  }

  List<BarChartData> getMonthUsage() {
    List<BarChartData> list = <BarChartData>[];
    list.add(BarChartData("Jan", 100));
    list.add(BarChartData("Feb", 70));
    list.add(BarChartData("Mar", 50));
    list.add(BarChartData("Apr", 30));
    list.add(BarChartData("May", 55));
    list.add(BarChartData("Jun", 26));
    list.add(BarChartData("Jul", 88));
    list.add(BarChartData("Aug", 35));
    list.add(BarChartData("Sep", 40));
    list.add(BarChartData("Oct", 125));
    list.add(BarChartData("Nov", 122));
    list.add(BarChartData("Dec", 99));
    return list;
  }
}
