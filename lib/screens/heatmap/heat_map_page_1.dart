import 'package:energy_analyzer/model/bar_chart_data.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

double? width;
double? height;

class HeatMapPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
      height: 400, //400
      child: Card(
          child: Column(
        children: [
          Flexible(
            child: ListTile(
              title: Text("Month Wise Usage"),
              subtitle: Text("KwH"),
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: HeatMap(),
            ),
            flex: 5,
          ),
        ],
      )),
    );
  }
}

class HeatMap extends StatelessWidget {
  final bool animate = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 7,
        ),
        scrollDirection: Axis.horizontal,
        children: getList(),
      ),
    );
  }

  List<Widget> getList() {
    List<Widget> list = [];
    for (int i = 1, j = 1; i <= 100; i++, j++) {
      switch (j) {
        case 1:
          list.add(Container(
            color: Colors.blue[100],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 2:
          list.add(Container(
            color: Colors.blue[200],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 3:
          list.add(Container(
            color: Colors.blue[300],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 4:
          list.add(Container(
            color: Colors.blue[400],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 5:
          list.add(Container(
            color: Colors.blue[500],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 6:
          list.add(Container(
            color: Colors.blue[600],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 7:
          list.add(Container(
            color: Colors.blue[700],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 8:
          list.add(Container(
            color: Colors.blue[800],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          break;
        case 9:
          list.add(Container(
            color: Colors.blue[900],
            child: Center(
              child: Text(
                "ABC",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));
          j = 1;
          break;
      }
    }
    return list;
  }
}
