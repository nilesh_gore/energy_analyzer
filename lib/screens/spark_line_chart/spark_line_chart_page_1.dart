import 'package:chart_sparkline/chart_sparkline.dart';

import 'package:flutter/material.dart';
import 'package:chart_sparkline/chart_sparkline.dart';

double? width;
double? height;

class SparkLineChartPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
      height: 300, //400
      child: Card(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: ListTile(
              title: Text("i-AM Zone"),
              subtitle: Text("Lower The Yesterday Average"),
            ),
            flex: 2,
          ),
          Flexible(
            child: Row(
              children: [
                Flexible(
                    child: ListTile(
                  title: Text("Today At 9:30AM"),
                  subtitle: Text("32°C"),
                )),
                Flexible(
                    child: ListTile(
                  title: Text("Today's Average"),
                  subtitle: Text("24°C"),
                )),
              ],
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(25, 25, 25, 25),
              child: Center(
                child: SparkLineChart(),
              ),
            ),
            flex: 5,
          ),
        ],
      )),
    );
  }
}

class SparkLineChart extends StatelessWidget {
  final bool animate = true;
  @override
  Widget build(BuildContext context) {
    return Sparkline(
        lineWidth: 5,
        lineColor: Colors.green,
        useCubicSmoothing: true,
        cubicSmoothingFactor: 0.2,
        data: [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0]);
  }
}
