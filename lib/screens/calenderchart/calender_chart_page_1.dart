import 'package:energy_analyzer/model/bar_chart_data.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:oktoast/oktoast.dart'; // 1. import library

double? width;
double? height;

class CalenderChartPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
      height: 550, //400
      child: Card(
          child: Column(
        children: [
          Flexible(
            child: ListTile(
              title: Text("System Status"),
              subtitle: Text("Monthly heartbeat data"),
            ),
            flex: 2,
          ),
          Flexible(
            child: ListTile(
              leading: IconButton(
                icon: Icon(Icons.west),
                onPressed: () {},
              ),
              title: Center(
                child: Text(
                  "July 2021",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              trailing: IconButton(
                icon: Icon(Icons.east),
                onPressed: () {},
              ),
            ),
            flex: 1,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: CalenderChart(),
            ),
            flex: 6,
          ),
        ],
      )),
    );
  }
}

class CalenderChart extends StatelessWidget {
  final bool animate = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView(
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 7,
        ),
        scrollDirection: Axis.vertical,
        children: getListNew(),
      ),
    );
  }

  List<Widget> getList() {
    List<Widget> list = [];
    for (int i = 1, j = 1; i <= 42; i++, j++) {
      switch (j) {
        case 1:
          list.add(GestureDetector(
            onTap: () {},
            child: Container(
              color: Colors.blue[100],
              child: Center(
                child: Text(
                  "$i",
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ),
          ));
          break;
        case 2:
          list.add(GestureDetector(
            onTap: () {},
            child: Container(
              color: Colors.blue[200],
              child: Center(
                child: Text(
                  "$i",
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ),
          ));
          break;
        case 3:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                color: Colors.blue[300],
                child: Center(
                  child: Text(
                    "$i",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 4:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                color: Colors.blue[400],
                child: Center(
                  child: Text(
                    "$i",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 5:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                color: Colors.blue[500],
                child: Center(
                  child: Text(
                    "$i",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));

          break;
        case 6:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                color: Colors.blue[600],
                child: Center(
                  child: Text(
                    "$i",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 7:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                color: Colors.blue[700],
                child: Center(
                  child: Text(
                    "$i",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 8:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                color: Colors.blue[800],
                child: Center(
                  child: Text(
                    "$i",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 9:
          list.add(Container(
            color: Colors.blue[900],
            child: Center(
              child: Text(
                "$i",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ));

          j = 1;
          break;
      }
    }
    return list;
  }

  List<Widget> getListNew() {
    List<Widget> list = [];
    for (int i = 1, j = 1; i <= 49; i++) {
      switch (i) {
        case 1:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Sun",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 2:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Mon",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 3:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Tue",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 4:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Wed",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 5:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Thu",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 6:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Fri",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        case 7:
          list.add(GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Text(
                    "Sat",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          break;
        default:
          list.add(GestureDetector(
              onTap: () async {
                print('Clicked.! ${j - 1}');
              },
              child: Container(
                color: Colors.blue[500],
                child: Center(
                  child: Text(
                    "$j",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              )));
          j++;
          break;
      }
    }
    return list;
  }
}
