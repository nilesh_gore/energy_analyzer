import 'package:energy_analyzer/model/bar_chart_data.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

double? width;
double? height;

class StackedBarChartPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
      height: 600, //400
      child: Card(
          child: Column(
        children: [
          Flexible(
            child: ListTile(
              title: Text("Today's Time Wise"),
              subtitle: Text("Unites"),
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: StackedBarChart(),
            ),
            flex: 5,
          ),
        ],
      )),
    );
  }
}

class StackedBarChart extends StatelessWidget {
  final bool animate = true;

  @override
  Widget build(BuildContext context) {
    return charts.BarChart(
      createChartData(),
      animate: animate,
      barGroupingType: charts.BarGroupingType.stacked,
      vertical: false,
      behaviors: [new charts.SeriesLegend()],
    );
  }

  List<charts.Series<BarChartData, String>> createChartData() {
    return [
      charts.Series<BarChartData, String>(
          id: 'Q1',
          domainFn: (BarChartData data, _) => data.month,
          measureFn: (BarChartData data, _) => data.usage,
          data: getUsageQ1(),
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault),
      charts.Series<BarChartData, String>(
          id: 'Q2',
          domainFn: (BarChartData data, _) => data.month,
          measureFn: (BarChartData data, _) => data.usage,
          data: getUsageQ2(),
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault),
      charts.Series<BarChartData, String>(
          id: 'Q3',
          domainFn: (BarChartData data, _) => data.month,
          measureFn: (BarChartData data, _) => data.usage,
          data: getUsageQ3(),
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault),
    ];
  }

  List<BarChartData> getUsageQ1() {
    List<BarChartData> list = <BarChartData>[];
    list.add(BarChartData("23:00", 10));
    list.add(BarChartData("22:00", 45));
    list.add(BarChartData("21:00", 94));
    list.add(BarChartData("20:00", 88));
    list.add(BarChartData("19:00", 100));
    list.add(BarChartData("18:00", 78));
    list.add(BarChartData("17:00", 67));
    list.add(BarChartData("16:00", 30));
    list.add(BarChartData("15:00", 110));
    list.add(BarChartData("14:00", 50));
    list.add(BarChartData("13:00", 40));
    list.add(BarChartData("12:00", 21));
    list.add(BarChartData("11:00", 34));
    list.add(BarChartData("10:00", 23));
    list.add(BarChartData("09:00", 20));
    list.add(BarChartData("08:00", 8));
    list.add(BarChartData("07:00", 4));
    list.add(BarChartData("06:00", 3));
    list.add(BarChartData("05:00", 1));
    list.add(BarChartData("04:00", 1));
    list.add(BarChartData("03:00", 4));
    list.add(BarChartData("02:00", 0));
    list.add(BarChartData("01:00", 2));
    list.add(BarChartData("00:00", 0));
    return list;
  }

  List<BarChartData> getUsageQ2() {
    List<BarChartData> list = <BarChartData>[];
    list.add(BarChartData("23:00", 44));
    list.add(BarChartData("22:00", 45));
    list.add(BarChartData("21:00", 67));
    list.add(BarChartData("20:00", 63));
    list.add(BarChartData("19:00", 18));
    list.add(BarChartData("18:00", 44));
    list.add(BarChartData("17:00", 88));
    list.add(BarChartData("16:00", 09));
    list.add(BarChartData("15:00", 45));
    list.add(BarChartData("14:00", 33));
    list.add(BarChartData("13:00", 64));
    list.add(BarChartData("12:00", 24));
    list.add(BarChartData("11:00", 54));
    list.add(BarChartData("10:00", 79));
    list.add(BarChartData("09:00", 34));
    list.add(BarChartData("08:00", 43));
    list.add(BarChartData("07:00", 23));
    list.add(BarChartData("06:00", 36));
    list.add(BarChartData("05:00", 10));
    list.add(BarChartData("04:00", 13));
    list.add(BarChartData("03:00", 41));
    list.add(BarChartData("02:00", 04));
    list.add(BarChartData("01:00", 27));
    list.add(BarChartData("00:00", 78));
    return list;
  }

  List<BarChartData> getUsageQ3() {
    List<BarChartData> list = <BarChartData>[];
    list.add(BarChartData("23:00", 32));
    list.add(BarChartData("22:00", 77));
    list.add(BarChartData("21:00", 12));
    list.add(BarChartData("20:00", 47));
    list.add(BarChartData("19:00", 90));
    list.add(BarChartData("18:00", 23));
    list.add(BarChartData("17:00", 34));
    list.add(BarChartData("16:00", 76));
    list.add(BarChartData("15:00", 44));
    list.add(BarChartData("14:00", 83));
    list.add(BarChartData("13:00", 56));
    list.add(BarChartData("12:00", 90));
    list.add(BarChartData("11:00", 100));
    list.add(BarChartData("10:00", 21));
    list.add(BarChartData("09:00", 12));
    list.add(BarChartData("08:00", 47));
    list.add(BarChartData("07:00", 34));
    list.add(BarChartData("06:00", 66));
    list.add(BarChartData("05:00", 49));
    list.add(BarChartData("04:00", 70));
    list.add(BarChartData("03:00", 12));
    list.add(BarChartData("02:00", 74));
    list.add(BarChartData("01:00", 0));
    list.add(BarChartData("00:00", 11));
    return list;
  }
}
