import 'package:energy_analyzer/model/bar_chart_data.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

double? width;
double? height;

class HorizontalBarChartPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
      height: 550, //400
      child: Card(
          child: Column(
        children: [
          Flexible(
            child: ListTile(
              title: Text("Hour Wise Usage"),
              subtitle: Text("KwH for Today"),
              trailing: IconButton(
                onPressed: () {},
                icon: Icon(Icons.more_vert),
              ),
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: HorizontalBarChart(),
            ),
            flex: 10,
          ),
        ],
      )),
    );
  }
}

class HorizontalBarChart extends StatelessWidget {
  final bool animate = true;

  @override
  Widget build(BuildContext context) {
    return charts.BarChart(
      createChartData(),
      animate: animate,
      vertical: false,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
      domainAxis: charts.OrdinalAxisSpec(
        renderSpec: charts.SmallTickRendererSpec(labelRotation: 0),
      ),
    );
  }

  List<charts.Series<BarChartData, String>> createChartData() {
    return [
      charts.Series<BarChartData, String>(
          id: 'Month Use',
          domainFn: (BarChartData data, _) => data.month,
          measureFn: (BarChartData data, _) => data.usage,
          data: getMonthUsage(),
          labelAccessorFn: (BarChartData data, _) => "",
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault),
    ];
  }

  List<BarChartData> getMonthUsage() {
    List<BarChartData> list = <BarChartData>[];
    list.add(BarChartData("23:00", 10));
    list.add(BarChartData("22:00", 45));
    list.add(BarChartData("21:00", 94));
    list.add(BarChartData("20:00", 88));
    list.add(BarChartData("19:00", 100));
    list.add(BarChartData("18:00", 78));
    list.add(BarChartData("17:00", 67));
    list.add(BarChartData("16:00", 30));
    list.add(BarChartData("15:00", 110));
    list.add(BarChartData("14:00", 50));
    list.add(BarChartData("13:00", 40));
    list.add(BarChartData("12:00", 21));
    list.add(BarChartData("11:00", 34));
    list.add(BarChartData("10:00", 23));
    list.add(BarChartData("09:00", 20));
    list.add(BarChartData("08:00", 8));
    list.add(BarChartData("07:00", 4));
    list.add(BarChartData("06:00", 3));
    list.add(BarChartData("05:00", 1));
    list.add(BarChartData("04:00", 1));
    list.add(BarChartData("03:00", 4));
    list.add(BarChartData("02:00", 0));
    list.add(BarChartData("01:00", 2));
    list.add(BarChartData("00:00", 0));
    return list;
  }
}
