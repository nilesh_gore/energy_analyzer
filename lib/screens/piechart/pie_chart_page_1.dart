import 'package:energy_analyzer/model/bar_chart_data.dart';
import 'package:energy_analyzer/model/pie_chart_data.dart';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

double? width;
double? height;

class PieChartPageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return Container(
      padding: EdgeInsets.fromLTRB(10, 15, 10, 10),
      height: 400, //400
      child: Card(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: ListTile(
              title: Text("Site Wise Usage"),
              subtitle: Text("KwH for January"),
              trailing: IconButton(
                onPressed: () {},
                icon: Icon(Icons.more_vert),
              ),
            ),
            flex: 2,
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Center(
                child: PieChart(),
              ),
            ),
            flex: 5,
          ),
        ],
      )),
    );
  }
}

class PieChart extends StatelessWidget {
  final bool animate = true;

  @override
  Widget build(BuildContext context) {
    return charts.PieChart<String>(createChartData(),
        animate: animate,
        defaultRenderer: new charts.ArcRendererConfig(
            arcWidth: 40,
            arcRendererDecorators: [new charts.ArcLabelDecorator()]));
  }

  List<charts.Series<PieChartData, String>> createChartData() {
    return [
      charts.Series<PieChartData, String>(
          id: 'Month Use',
          domainFn: (PieChartData data, _) => data.site,
          measureFn: (PieChartData data, _) => data.usage,
          data: getMonthUsage(),
          // labelAccessorFn: (PieChartData row, _) => '${row.site}',
          colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault),
    ];
  }

  List<PieChartData> getMonthUsage() {
    List<PieChartData> list = <PieChartData>[];
    list.add(PieChartData("Pune", 18));
    list.add(PieChartData("Mumbai", 30));
    list.add(PieChartData("Nagpur", 2));
    list.add(PieChartData("Nashik", 25));
    list.add(PieChartData("Kolhapur", 25));
    return list;
  }
}
