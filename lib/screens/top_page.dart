import 'package:flutter/material.dart';
import 'package:page_view_indicators/page_view_indicators.dart';

double? width;
double? height;

class TopPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TopPageState();
  }
}

class TopPageState extends State<TopPage> {
  final pageController = PageController();
  final currentPageNotifier = ValueNotifier<int>(0);
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.shortestSide;
    height = MediaQuery.of(context).size.longestSide;
    return ClipPath(
      clipper: Clipper08(),
      child: Container(
          height: height! * .65 < 250 ? height! * .65 : 300,
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Colors.green,
            Colors.green,
          ])),
          child: Column(
            children: [
              buildPageView(),
              buildCircleIndicator(),
            ],
          )),
    );
  }

  buildPageView() {
    return Container(
      height: height! * .65 < 200 ? height! * .65 : 225,
      width: width! * .85 < 500 ? width! * .85 : 575,
      child: PageView.builder(
          itemCount: 5,
          controller: pageController,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      title: Text(
                        "i-AM Zone (${index + 1})",
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        "Today's",
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "80 Unites",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "30% Higher Then Yesterday",
                            style: TextStyle(
                              color: Colors.grey[500],
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      title: Text(
                        "Month's",
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "700 Unites",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "30% Higher Then Month Average",
                            style: TextStyle(
                              color: Colors.grey[500],
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
          onPageChanged: (int index) {
            currentPageNotifier.value = index;
          }),
    );
  }

  buildCircleIndicator() {
    return Padding(
      padding: EdgeInsets.fromLTRB(5, 15, 5, 10),
      child: CirclePageIndicator(
        itemCount: 5,
        selectedSize: 12,
        dotColor: Colors.white,
        selectedDotColor: Colors.green[900],
        currentPageNotifier: currentPageNotifier,
      ),
    );
  }
}

class Clipper08 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path = Path();
    path.lineTo(0.0, size.height);
    // ignore: non_constant_identifier_names
    var End = Offset(size.width / 2, size.height - 30.0);
    // ignore: non_constant_identifier_names
    var Control = Offset(size.width / 4, size.height - 50.0);

    path.quadraticBezierTo(Control.dx, Control.dy, End.dx, End.dy);
    // ignore: non_constant_identifier_names
    var End2 = Offset(size.width, size.height - 80.0);
    // ignore: non_constant_identifier_names
    var Control2 = Offset(size.width * .75, size.height - 10.0);

    path.quadraticBezierTo(Control2.dx, Control2.dy, End2.dx, End2.dy);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
